// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

//UE4_PROJECT_STEAMSHIPPINGID

public class CrazyAssylumTarget : TargetRules
{
	public CrazyAssylumTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "CrazyAssylum" } );
	}
}
